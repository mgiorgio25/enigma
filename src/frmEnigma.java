//import com.sun.glass.events.KeyEvent;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
//Libreria per generazione dei numeri
import java.util.Random;
import javax.swing.JOptionPane;
import java.io.*;
import javax.swing.filechooser.FileFilter;

public class frmEnigma extends javax.swing.JFrame 
{
    Random random=new Random();
    //Dichiarazione di tutti i rotori
    int rotore1[], rotore2[], rotore3[], specchio[], rotore1_1[], rotore2_2[], rotore3_3[];
    //Variabili che indicano la posizione dei rotori
    int pos1, pos2, pos3;
    //Variabile che contiene la lettera digitata
    char lettera;
    //Vettore che contiene l'alfabeto
    char alfabeto[]={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
    //Vettori statici per lo specchio
    int specchio_1[]={8,6,9,10,1,7,5,3,1,3,10,9,2,5,6,4,8,7,11,12,11,13,12,2,4,13};
    int specchio_2[]={7,5,8,9,2,6,4,2,1,3,9,8,1,4,5,13,7,6,10,11,10,12,11,13,3,12};
    int specchio_3[]={9,6,7,10,3,7,5,3,2,4,10,9,2,5,6,13,8,1,11,12,11,13,12,8,4,1};
    //Variabile per contatore i giri del rotore1
    int cont=0;
    //Variabile per contatore i giri del rotore2
    int cont1=0;
    public frmEnigma() 
    {
        initComponents();
        this.requestFocus();
        this.getContentPane().setBackground(new Color(204, 102, 0));
        rotore1=new int[26];
        rotore1_1=new int[26];
        rotore2=new int [26];
        rotore2_2=new int[26];
        rotore3=new int[26];
        rotore3_3=new int[26];
        specchio=new int[26];
        
        //Calcola la dimensione dello schermo
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize ();
        //Calcola la dimensione della finestra
        Dimension frameSize = this.getSize ();
        //Imposta la posizione della finestra
        this.setLocation ((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
        //Icona del software
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("images/enigma1.png")));
        CaricaRotori();
        txtRotore1.setText(""+alfabeto[pos1]);
        txtRotore2.setText(""+alfabeto[pos2]);
        txtRotore3.setText(""+alfabeto[pos3]);
    }
    private void CaricaRotori()
    {
        int i, var=0, newGeneration=0, j, contatore=0;
        for(i=0;i<26;i++)
        {
            rotore1[i]=i+1;
            rotore2[i]=i+1;
            rotore3[i]=i+1;
        }
        pos1=random.nextInt(25);
        pos2=random.nextInt(25);
        pos3=random.nextInt(25);
        //Caricamento del vettore interno del Rotore 1
        for(i=0;i<26;i++)
        {
            newGeneration=0;
            while(newGeneration==0)
            {
                newGeneration=1;
                var=random.nextInt(27);
                if(var==0) newGeneration=0;
                for(j=0;j<26;j++)
                {
                    if(rotore1_1[j]==var) newGeneration=0;
                }
                rotore1_1[i]=var;
            }
            
        }
        //Caricamento del vettore interno del Rotore 2
        for(i=0;i<26;i++)
        {
            while(newGeneration==0)
            {
                newGeneration=1;
                var=random.nextInt(27);
                if(var==0) newGeneration=0;
                for(j=0;j<26;j++)
                {
                    if(rotore2_2[j]==var) newGeneration=0;
                }
            }
            newGeneration=0;
            rotore2_2[i]=var;
        }
        //Caricamento del vettore interno del Rotore 3
        for(i=0;i<26;i++)
        {
            while(newGeneration==0)
            {
                newGeneration=1;
                var=random.nextInt(27);
                if(var==0) newGeneration=0;
                for(j=0;j<26;j++)
                {
                    if(rotore3_3[j]==var) newGeneration=0;
                }
            }
            newGeneration=0;
            rotore3_3[i]=var;
        }
        var=random.nextInt(2);
        if(var==0) specchio=specchio_1;
        else if(var==1) specchio=specchio_2;
        else if(var==2) specchio=specchio_3;
    }
    private void Encripta()
    {
        //Variabile per la lettera codificata
        char lettera1;
        int pos=0, pos1=0, pos2=0;
        lettera=Character.toUpperCase(lettera);
        for(int i=0;i<26;i++)
            if(lettera==alfabeto[i])
                //Mi salvo nella variabile pos, la posizione della lettera
                pos=i;
        pos=rotore1[pos];
        for(int i=0;i<26;i++)
            if(rotore1_1[i]==pos)
                pos1=i;
        pos=rotore2[pos1];
        for(int i=0;i<26;i++)
            if(rotore2_2[i]==pos)
                pos1=i;
        pos=rotore3[pos1];
        for(int i=0;i<26;i++)
            if(rotore3_3[i]==pos)
                pos1=i;
        pos=specchio[pos1];
        pos2=pos1;
        for(int i=0;i<26;i++)
            if(specchio[i]==pos && i!=pos2)
                pos1=i;
        pos=rotore3_3[pos1];
        for(int i=0;i<26;i++)
            if(rotore3[i]==pos)
                pos=i;
        pos1=rotore2_2[pos];
        for(int i=0;i<26;i++)
            if(rotore2[i]==pos1)
                pos=i;
        pos1=rotore1_1[pos];
        for(int i=0;i<26;i++)
            if(rotore1[i]==pos1)
                pos=i;
        lettera1=alfabeto[pos];
        txtRis.setText(txtRis.getText()+lettera1);
        IlluminazionePulsante IP = new IlluminazionePulsante(lettera1);
        Thread t = new Thread(IP);
        t.start();
        btnRotore1piuActionPerformed(null);
        cont++;
        if(cont==26)
        { 
            btnRotore2piuActionPerformed(null);
            cont1++;
            cont=0;
        }
        if(cont1==26)
        {
            btnRotore3piuActionPerformed(null);
            cont1=0;
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollBar1 = new javax.swing.JScrollBar();
        FileSalvaImposta = new javax.swing.JFileChooser();
        txtRotore1 = new javax.swing.JTextField();
        txtRotore2 = new javax.swing.JTextField();
        txtRotore3 = new javax.swing.JTextField();
        btnRotore1piu = new javax.swing.JButton();
        btnRotore1meno = new javax.swing.JButton();
        btnRotore2piu = new javax.swing.JButton();
        btnRotore2meno = new javax.swing.JButton();
        btnRotore3meno = new javax.swing.JButton();
        btnRotore3piu = new javax.swing.JButton();
        btnA = new JButtonRotondo();
        btnB = new JButtonRotondo();
        btnC = new JButtonRotondo();
        btnD = new JButtonRotondo();
        btnE = new JButtonRotondo();
        btnF = new JButtonRotondo();
        btnG = new JButtonRotondo();
        btnH = new JButtonRotondo();
        btnI = new JButtonRotondo();
        btnL = new JButtonRotondo();
        btnM = new JButtonRotondo();
        btnO = new JButtonRotondo();
        btnP = new JButtonRotondo();
        btnQ = new JButtonRotondo();
        btnJ = new JButtonRotondo();
        btnK = new JButtonRotondo();
        btnN = new JButtonRotondo();
        btnR = new JButtonRotondo();
        btnS = new JButtonRotondo();
        btnT = new JButtonRotondo();
        btnU = new JButtonRotondo();
        btnV = new JButtonRotondo();
        btnW = new JButtonRotondo();
        btnX = new JButtonRotondo();
        btnY = new JButtonRotondo();
        btnZ = new JButtonRotondo();
        btnM1 = new JButtonRotondo();
        btnO1 = new JButtonRotondo();
        btnP1 = new JButtonRotondo();
        btnQ1 = new JButtonRotondo();
        btnJ1 = new JButtonRotondo();
        btnK1 = new JButtonRotondo();
        btnN1 = new JButtonRotondo();
        btnR1 = new JButtonRotondo();
        btnS1 = new JButtonRotondo();
        btnT1 = new JButtonRotondo();
        btnU1 = new JButtonRotondo();
        btnV1 = new JButtonRotondo();
        btnA1 = new JButtonRotondo();
        btnW1 = new JButtonRotondo();
        btnB1 = new JButtonRotondo();
        btnX1 = new JButtonRotondo();
        btnC1 = new JButtonRotondo();
        btnY1 = new JButtonRotondo();
        btnD1 = new JButtonRotondo();
        btnZ1 = new JButtonRotondo();
        btnE1 = new JButtonRotondo();
        btnF1 = new JButtonRotondo();
        btnG1 = new JButtonRotondo();
        btnH1 = new JButtonRotondo();
        btnI1 = new JButtonRotondo();
        btnL1 = new JButtonRotondo();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtRis = new javax.swing.JTextArea();
        btnSalva = new javax.swing.JButton();
        btnImposta = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Enigma");
        setBackground(new java.awt.Color(204, 102, 0));
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        txtRotore1.setEditable(false);

        txtRotore2.setEditable(false);

        txtRotore3.setEditable(false);

        btnRotore1piu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/+.gif"))); // NOI18N
        btnRotore1piu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRotore1piuActionPerformed(evt);
            }
        });

        btnRotore1meno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/-.png"))); // NOI18N
        btnRotore1meno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRotore1menoActionPerformed(evt);
            }
        });

        btnRotore2piu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/+.gif"))); // NOI18N
        btnRotore2piu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRotore2piuActionPerformed(evt);
            }
        });

        btnRotore2meno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/-.png"))); // NOI18N
        btnRotore2meno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRotore2menoActionPerformed(evt);
            }
        });

        btnRotore3meno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/-.png"))); // NOI18N
        btnRotore3meno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRotore3menoActionPerformed(evt);
            }
        });

        btnRotore3piu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/+.gif"))); // NOI18N
        btnRotore3piu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRotore3piuActionPerformed(evt);
            }
        });

        btnA.setText("A");
        btnA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAActionPerformed(evt);
            }
        });

        btnB.setText("B");
        btnB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBActionPerformed(evt);
            }
        });

        btnC.setText("C");
        btnC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCActionPerformed(evt);
            }
        });

        btnD.setText("D");
        btnD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDActionPerformed(evt);
            }
        });

        btnE.setText("E");
        btnE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEActionPerformed(evt);
            }
        });

        btnF.setText("F");
        btnF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFActionPerformed(evt);
            }
        });

        btnG.setText("G");
        btnG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGActionPerformed(evt);
            }
        });

        btnH.setText("H");
        btnH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHActionPerformed(evt);
            }
        });

        btnI.setText("I");
        btnI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIActionPerformed(evt);
            }
        });

        btnL.setText("L");
        btnL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLActionPerformed(evt);
            }
        });

        btnM.setText("M");
        btnM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMActionPerformed(evt);
            }
        });

        btnO.setText("O");
        btnO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOActionPerformed(evt);
            }
        });

        btnP.setText("P");
        btnP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPActionPerformed(evt);
            }
        });

        btnQ.setText("Q");
        btnQ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQActionPerformed(evt);
            }
        });

        btnJ.setText("J");
        btnJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJActionPerformed(evt);
            }
        });

        btnK.setText("K");
        btnK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKActionPerformed(evt);
            }
        });

        btnN.setText("N");
        btnN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNActionPerformed(evt);
            }
        });

        btnR.setText("R");
        btnR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRActionPerformed(evt);
            }
        });

        btnS.setText("S");
        btnS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSActionPerformed(evt);
            }
        });

        btnT.setText("T");
        btnT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTActionPerformed(evt);
            }
        });

        btnU.setText("U");
        btnU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUActionPerformed(evt);
            }
        });

        btnV.setText("V");
        btnV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVActionPerformed(evt);
            }
        });

        btnW.setText("W");
        btnW.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnWActionPerformed(evt);
            }
        });

        btnX.setText("X");
        btnX.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXActionPerformed(evt);
            }
        });

        btnY.setText("Y");
        btnY.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnYActionPerformed(evt);
            }
        });

        btnZ.setText("Z");
        btnZ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnZActionPerformed(evt);
            }
        });

        btnM1.setBackground(new java.awt.Color(255, 255, 255));
        btnM1.setForeground(new java.awt.Color(0, 0, 0));
        btnM1.setText("M");
        btnM1.setFocusable(false);
        btnM1.setRequestFocusEnabled(false);

        btnO1.setBackground(new java.awt.Color(255, 255, 255));
        btnO1.setForeground(new java.awt.Color(0, 0, 0));
        btnO1.setText("O");
        btnO1.setFocusable(false);
        btnO1.setRequestFocusEnabled(false);

        btnP1.setBackground(new java.awt.Color(255, 255, 255));
        btnP1.setForeground(new java.awt.Color(0, 0, 0));
        btnP1.setText("P");
        btnP1.setFocusable(false);
        btnP1.setRequestFocusEnabled(false);

        btnQ1.setBackground(new java.awt.Color(255, 255, 255));
        btnQ1.setForeground(new java.awt.Color(0, 0, 0));
        btnQ1.setText("Q");
        btnQ1.setFocusable(false);
        btnQ1.setRequestFocusEnabled(false);

        btnJ1.setBackground(new java.awt.Color(255, 255, 255));
        btnJ1.setForeground(new java.awt.Color(0, 0, 0));
        btnJ1.setText("J");
        btnJ1.setFocusable(false);
        btnJ1.setRequestFocusEnabled(false);

        btnK1.setBackground(new java.awt.Color(255, 255, 255));
        btnK1.setForeground(new java.awt.Color(0, 0, 0));
        btnK1.setText("K");
        btnK1.setFocusable(false);
        btnK1.setRequestFocusEnabled(false);

        btnN1.setBackground(new java.awt.Color(255, 255, 255));
        btnN1.setForeground(new java.awt.Color(0, 0, 0));
        btnN1.setText("N");
        btnN1.setFocusable(false);
        btnN1.setRequestFocusEnabled(false);

        btnR1.setBackground(new java.awt.Color(255, 255, 255));
        btnR1.setForeground(new java.awt.Color(0, 0, 0));
        btnR1.setText("R");
        btnR1.setFocusable(false);
        btnR1.setRequestFocusEnabled(false);

        btnS1.setBackground(new java.awt.Color(255, 255, 255));
        btnS1.setForeground(new java.awt.Color(0, 0, 0));
        btnS1.setText("S");
        btnS1.setFocusable(false);
        btnS1.setRequestFocusEnabled(false);

        btnT1.setBackground(new java.awt.Color(255, 255, 255));
        btnT1.setForeground(new java.awt.Color(0, 0, 0));
        btnT1.setText("T");
        btnT1.setFocusable(false);
        btnT1.setRequestFocusEnabled(false);

        btnU1.setBackground(new java.awt.Color(255, 255, 255));
        btnU1.setForeground(new java.awt.Color(0, 0, 0));
        btnU1.setText("U");
        btnU1.setFocusable(false);
        btnU1.setRequestFocusEnabled(false);

        btnV1.setBackground(new java.awt.Color(255, 255, 255));
        btnV1.setForeground(new java.awt.Color(0, 0, 0));
        btnV1.setText("V");
        btnV1.setFocusable(false);
        btnV1.setRequestFocusEnabled(false);

        btnA1.setBackground(new java.awt.Color(255, 255, 255));
        btnA1.setForeground(new java.awt.Color(0, 0, 0));
        btnA1.setText("A");
        btnA1.setFocusable(false);
        btnA1.setRequestFocusEnabled(false);

        btnW1.setBackground(new java.awt.Color(255, 255, 255));
        btnW1.setForeground(new java.awt.Color(0, 0, 0));
        btnW1.setText("W");
        btnW1.setFocusable(false);
        btnW1.setRequestFocusEnabled(false);

        btnB1.setBackground(new java.awt.Color(255, 255, 255));
        btnB1.setForeground(new java.awt.Color(0, 0, 0));
        btnB1.setText("B");
        btnB1.setFocusable(false);
        btnB1.setRequestFocusEnabled(false);

        btnX1.setBackground(new java.awt.Color(255, 255, 255));
        btnX1.setForeground(new java.awt.Color(0, 0, 0));
        btnX1.setText("X");
        btnX1.setFocusable(false);
        btnX1.setRequestFocusEnabled(false);

        btnC1.setBackground(new java.awt.Color(255, 255, 255));
        btnC1.setForeground(new java.awt.Color(0, 0, 0));
        btnC1.setText("C");
        btnC1.setFocusable(false);
        btnC1.setRequestFocusEnabled(false);

        btnY1.setBackground(new java.awt.Color(255, 255, 255));
        btnY1.setForeground(new java.awt.Color(0, 0, 0));
        btnY1.setText("Y");
        btnY1.setFocusable(false);
        btnY1.setRequestFocusEnabled(false);

        btnD1.setBackground(new java.awt.Color(255, 255, 255));
        btnD1.setForeground(new java.awt.Color(0, 0, 0));
        btnD1.setText("D");
        btnD1.setFocusable(false);
        btnD1.setRequestFocusEnabled(false);

        btnZ1.setBackground(new java.awt.Color(255, 255, 255));
        btnZ1.setForeground(new java.awt.Color(0, 0, 0));
        btnZ1.setText("Z");
        btnZ1.setFocusable(false);
        btnZ1.setRequestFocusEnabled(false);

        btnE1.setBackground(new java.awt.Color(255, 255, 255));
        btnE1.setForeground(new java.awt.Color(0, 0, 0));
        btnE1.setText("E");
        btnE1.setFocusable(false);
        btnE1.setRequestFocusEnabled(false);

        btnF1.setBackground(new java.awt.Color(255, 255, 255));
        btnF1.setForeground(new java.awt.Color(0, 0, 0));
        btnF1.setText("F");
        btnF1.setFocusable(false);
        btnF1.setRequestFocusEnabled(false);

        btnG1.setBackground(new java.awt.Color(255, 255, 255));
        btnG1.setForeground(new java.awt.Color(0, 0, 0));
        btnG1.setText("G");
        btnG1.setFocusable(false);
        btnG1.setRequestFocusEnabled(false);

        btnH1.setBackground(new java.awt.Color(255, 255, 255));
        btnH1.setForeground(new java.awt.Color(0, 0, 0));
        btnH1.setText("H");
        btnH1.setFocusable(false);
        btnH1.setRequestFocusEnabled(false);

        btnI1.setBackground(new java.awt.Color(255, 255, 255));
        btnI1.setForeground(new java.awt.Color(0, 0, 0));
        btnI1.setText("I");
        btnI1.setFocusable(false);
        btnI1.setRequestFocusEnabled(false);

        btnL1.setBackground(new java.awt.Color(255, 255, 255));
        btnL1.setForeground(new java.awt.Color(0, 0, 0));
        btnL1.setText("L");
        btnL1.setFocusable(false);
        btnL1.setRequestFocusEnabled(false);

        txtRis.setEditable(false);
        txtRis.setColumns(20);
        txtRis.setLineWrap(true);
        txtRis.setRows(5);
        jScrollPane1.setViewportView(txtRis);

        btnSalva.setText("SALVA ROTORI");
        btnSalva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvaActionPerformed(evt);
            }
        });

        btnImposta.setText("CARICA ROTORI");
        btnImposta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImpostaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(btnSalva, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(72, 72, 72)
                            .addComponent(txtRotore1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnRotore1piu, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnRotore1meno, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(txtRotore2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnRotore2piu, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnRotore2meno, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(txtRotore3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnRotore3meno, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnRotore3piu, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnImposta, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 662, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(37, 37, 37)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnA1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnS1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnD1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnF1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnG1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnH1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnJ1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnK1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(60, 60, 60)
                                        .addComponent(btnZ1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnX1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnC1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnV1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnB1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnN1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnM1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addComponent(btnL1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnQ1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnW1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnE1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnR1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnT1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnY1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnU1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnI1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnO1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnP1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnQ, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnW, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnE, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnR, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnT, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnY, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnU, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnI, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnO, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnP, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(37, 37, 37)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnA, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnS, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnD, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnF, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnG, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnH, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnJ, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnK, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(60, 60, 60)
                                        .addComponent(btnZ, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnX, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnC, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnV, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnB, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnN, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnM, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addComponent(btnL, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(50, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(btnRotore3piu, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnRotore3meno, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(btnRotore2piu, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnRotore2meno, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtRotore1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSalva, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtRotore2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtRotore3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(btnRotore1piu, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnRotore1meno, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnImposta, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnQ1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnW1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnE1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnR1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnT1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnY1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnU1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnI1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnO1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnP1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnL1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnK1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnJ1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnH1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnG1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnF1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnD1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnS1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnA1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnZ1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnC1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnB1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnM1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnX1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnV1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnN1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnQ, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnW, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnE, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnR, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnT, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnY, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnU, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnI, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnO, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnP, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnL, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnK, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnJ, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnH, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnG, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnF, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnD, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnS, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnA, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnZ, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnC, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnB, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnM, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnX, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnV, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnN, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        lettera=evt.getKeyChar();
        //Switch di controllo sul tasto premuto
        switch(lettera)
        {
            case 'q':
            case 'w':
            case 'e':
            case 'r':
            case 't':
            case 'y':
            case 'u':
            case 'i':
            case 'o':
            case 'p':
            case 'a':
            case 's':
            case 'd':
            case 'f':
            case 'g':
            case 'h':
            case 'j':
            case 'k':
            case 'l':
            case 'z':
            case 'x':
            case 'c':
            case 'v':
            case 'b':
            case 'n':
            case 'm':
                Encripta();
                break;
        }
        
    }//GEN-LAST:event_formKeyPressed

    private void btnRotore1piuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRotore1piuActionPerformed
        int contenutoElemento=rotore1_1[0];
        for(int i=0;i<25;i++)
            rotore1_1[i]=rotore1_1[i+1];
        rotore1_1[25]=contenutoElemento;
        contenutoElemento=rotore1_1[0];
        txtRotore1.setText(""+alfabeto[contenutoElemento-1]);
    }//GEN-LAST:event_btnRotore1piuActionPerformed

    private void btnRotore1menoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRotore1menoActionPerformed
        int contenutoElemento=rotore1_1[25];
        for(int i=25;i<0;i--)
            rotore1_1[i]=rotore1_1[i-1];
        rotore1_1[25]=contenutoElemento;
        contenutoElemento=rotore1_1[25];
        txtRotore1.setText(""+alfabeto[contenutoElemento-1]);
    }//GEN-LAST:event_btnRotore1menoActionPerformed

    private void btnRotore2piuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRotore2piuActionPerformed
        int contenutoElemento=rotore2_2[0];
        for(int i=0;i<25;i++)
            rotore2_2[i]=rotore2_2[i+1];
        rotore2_2[25]=contenutoElemento;
        contenutoElemento=rotore2_2[0];
        txtRotore2.setText(""+alfabeto[contenutoElemento-1]);
    }//GEN-LAST:event_btnRotore2piuActionPerformed

    private void btnRotore2menoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRotore2menoActionPerformed
        int contenutoElemento=rotore2_2[25];
        for(int i=25;i<0;i--)
            rotore2_2[i]=rotore2_2[i-1];
        rotore2_2[25]=contenutoElemento;
        contenutoElemento=rotore2_2[25];
        txtRotore2.setText(""+alfabeto[contenutoElemento-1]);
    }//GEN-LAST:event_btnRotore2menoActionPerformed

    private void btnRotore3piuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRotore3piuActionPerformed
        int contenutoElemento=rotore3_3[0];
        for(int i=0;i<25;i++)
            rotore3_3[i]=rotore3_3[i+1];
        rotore3_3[25]=contenutoElemento;
        contenutoElemento=rotore3_3[0];
        txtRotore3.setText(""+alfabeto[contenutoElemento-1]);
    }//GEN-LAST:event_btnRotore3piuActionPerformed

    private void btnRotore3menoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRotore3menoActionPerformed
        int contenutoElemento=rotore3_3[25];
        for(int i=25;i<0;i--)
            rotore3_3[i]=rotore3_3[i-1];
        rotore3_3[25]=contenutoElemento;
        contenutoElemento=rotore3_3[25];
        txtRotore3.setText(""+alfabeto[contenutoElemento-1]);
    }//GEN-LAST:event_btnRotore3menoActionPerformed

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        this.requestFocus();
    }//GEN-LAST:event_formMouseClicked

    private void btnQActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQActionPerformed
        lettera='Q';
        Encripta();
    }//GEN-LAST:event_btnQActionPerformed

    private void btnWActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnWActionPerformed
        lettera='W';
        Encripta();
    }//GEN-LAST:event_btnWActionPerformed

    private void btnEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEActionPerformed
        lettera='E';
        Encripta();
    }//GEN-LAST:event_btnEActionPerformed

    private void btnRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRActionPerformed
        lettera='R';
        Encripta();
    }//GEN-LAST:event_btnRActionPerformed

    private void btnTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTActionPerformed
        lettera='T';
        Encripta();
    }//GEN-LAST:event_btnTActionPerformed

    private void btnYActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnYActionPerformed
        lettera='Y';
        Encripta();
    }//GEN-LAST:event_btnYActionPerformed

    private void btnUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUActionPerformed
        lettera='U';
        Encripta();
    }//GEN-LAST:event_btnUActionPerformed

    private void btnIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIActionPerformed
        lettera='I';
        Encripta();
    }//GEN-LAST:event_btnIActionPerformed

    private void btnOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOActionPerformed
        lettera='O';
        Encripta();
    }//GEN-LAST:event_btnOActionPerformed

    private void btnPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPActionPerformed
        lettera='P';
        Encripta();
    }//GEN-LAST:event_btnPActionPerformed

    private void btnAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAActionPerformed
        lettera='A';
        Encripta();
    }//GEN-LAST:event_btnAActionPerformed

    private void btnSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSActionPerformed
        lettera='S';
        Encripta();
    }//GEN-LAST:event_btnSActionPerformed

    private void btnDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDActionPerformed
        lettera='D';
        Encripta();
    }//GEN-LAST:event_btnDActionPerformed

    private void btnFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFActionPerformed
        lettera='F';
        Encripta();
    }//GEN-LAST:event_btnFActionPerformed

    private void btnGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGActionPerformed
        lettera='G';
        Encripta();
    }//GEN-LAST:event_btnGActionPerformed

    private void btnHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHActionPerformed
        lettera='H';
        Encripta();
    }//GEN-LAST:event_btnHActionPerformed

    private void btnJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJActionPerformed
        lettera='J';
        Encripta();
    }//GEN-LAST:event_btnJActionPerformed

    private void btnKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKActionPerformed
        lettera='K';
        Encripta();
    }//GEN-LAST:event_btnKActionPerformed

    private void btnLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLActionPerformed
        lettera='L';
        Encripta();
    }//GEN-LAST:event_btnLActionPerformed

    private void btnZActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnZActionPerformed
        lettera='Z';
        Encripta();
    }//GEN-LAST:event_btnZActionPerformed

    private void btnXActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXActionPerformed
        lettera='X';
        Encripta();
    }//GEN-LAST:event_btnXActionPerformed

    private void btnCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCActionPerformed
        lettera='C';
        Encripta();
    }//GEN-LAST:event_btnCActionPerformed

    private void btnVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVActionPerformed
        lettera='V';
        Encripta();
    }//GEN-LAST:event_btnVActionPerformed

    private void btnBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBActionPerformed
        lettera='B';
        Encripta();
    }//GEN-LAST:event_btnBActionPerformed

    private void btnNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNActionPerformed
        lettera='N';
        Encripta();
    }//GEN-LAST:event_btnNActionPerformed

    private void btnMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMActionPerformed
        lettera='M';
        Encripta();
    }//GEN-LAST:event_btnMActionPerformed

    private void btnSalvaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvaActionPerformed
        txtRis.setText("");
        int fileSelezionato;
        String nomeFile;
        File fileScelto=null;
        FileSalvaImposta.addChoosableFileFilter(new FileFilter() 
        {
        @Override
        public String getDescription() 
        {
            return "Dat File (*.dat)";
        }
        @Override
        public boolean accept(File f) 
        {
            if (f.isDirectory()) 
            {
                return false;
            } 
            else 
            {
                return f.getName().toLowerCase().endsWith(".dat");
            }
        }
        });
        fileSelezionato=FileSalvaImposta.showSaveDialog(frmEnigma.this);
        if(fileSelezionato==FileSalvaImposta.APPROVE_OPTION)
            fileScelto=FileSalvaImposta.getSelectedFile();
        nomeFile= fileScelto.toString();
        if (!nomeFile.endsWith(".dat"))
            nomeFile += ".dat";
        try
        {
            FileOutputStream F=new FileOutputStream(nomeFile,true);
            ObjectOutputStream FOUT=new ObjectOutputStream(F);
            FOUT.writeObject(pos1);
            FOUT.writeObject(pos2);
            FOUT.writeObject(pos3);
            FOUT.writeObject(rotore1_1);
            FOUT.writeObject(rotore2_2);
            FOUT.writeObject(rotore3_3);
            FOUT.writeObject(specchio_1);
            FOUT.flush();
            JOptionPane.showMessageDialog(this, "Posizione dei rotori salvati nel file '"+nomeFile+"'");
            F.close();
        }
        catch(Exception e)
        {
            JOptionPane.showInputDialog(this,e.getMessage(),"Errore");
        }
    }//GEN-LAST:event_btnSalvaActionPerformed

    private void btnImpostaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImpostaActionPerformed
        txtRis.setText("");
        int fileSelezionato;
        File fileScelto=null;
        FileSalvaImposta.addChoosableFileFilter(new FileFilter() 
        {
        public String getDescription() 
        {
            return "Dat File (*.dat)";
        }
        public boolean accept(File f) 
        {
            if (f.isDirectory()) 
            {
                return true;
            } 
            else 
            {
                return f.getName().toLowerCase().endsWith(".dat");
            }
        }
        });
        fileSelezionato=FileSalvaImposta.showOpenDialog(frmEnigma.this);
        if(fileSelezionato==FileSalvaImposta.APPROVE_OPTION)
            fileScelto=FileSalvaImposta.getSelectedFile();
        try
        {
            FileInputStream F=new FileInputStream(fileScelto);
            while(true)
            {
                try
                {
                    ObjectInputStream FIN=new ObjectInputStream(F);
                    for(int i=0;i<7;i++)
                    {
                        if(i==0)
                        {
                            pos1=(int)FIN.readObject();
                        }
                        else if(i==1)
                        {
                            pos2=(int)FIN.readObject();
                        }
                        else if(i==2)
                        {
                            pos3=(int)FIN.readObject();
                        }
                        else if(i==3)
                        {
                            rotore1_1=(int[])FIN.readObject();
                        }
                        else if(i==4)
                        {
                            rotore2_2=(int[])FIN.readObject();
                        }
                        else if(i==5)
                        {
                            rotore3_3=(int[])FIN.readObject();
                        }
                        else if(i==6)
                        {
                            specchio=(int[])FIN.readObject();
                        }
                    }
                }
                catch(EOFException e)
                {
                    break;
                }
            }
            F.close();
        }
        catch(Exception e)
        {
            JOptionPane.showInputDialog(this, e.getMessage(), "Errore");
        }
        txtRotore1.setText(""+alfabeto[pos1]);
        txtRotore2.setText(""+alfabeto[pos2]);
        txtRotore3.setText(""+alfabeto[pos3]);
    }//GEN-LAST:event_btnImpostaActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmEnigma.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmEnigma.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmEnigma.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmEnigma.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                frmEnigma f =new frmEnigma();
                f.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser FileSalvaImposta;
    private JButtonRotondo btnA;
    public static JButtonRotondo btnA1;
    private JButtonRotondo btnB;
    public static JButtonRotondo btnB1;
    private JButtonRotondo btnC;
    public static JButtonRotondo btnC1;
    private JButtonRotondo btnD;
    public static JButtonRotondo btnD1;
    private JButtonRotondo btnE;
    public static JButtonRotondo btnE1;
    private JButtonRotondo btnF;
    public static JButtonRotondo btnF1;
    private JButtonRotondo btnG;
    public static JButtonRotondo btnG1;
    private JButtonRotondo btnH;
    public static JButtonRotondo btnH1;
    private JButtonRotondo btnI;
    public static JButtonRotondo btnI1;
    private javax.swing.JButton btnImposta;
    private JButtonRotondo btnJ;
    public static JButtonRotondo btnJ1;
    private JButtonRotondo btnK;
    public static JButtonRotondo btnK1;
    private JButtonRotondo btnL;
    public static JButtonRotondo btnL1;
    private JButtonRotondo btnM;
    public static JButtonRotondo btnM1;
    private JButtonRotondo btnN;
    public static JButtonRotondo btnN1;
    private JButtonRotondo btnO;
    public static JButtonRotondo btnO1;
    private JButtonRotondo btnP;
    public static JButtonRotondo btnP1;
    private JButtonRotondo btnQ;
    public static JButtonRotondo btnQ1;
    private JButtonRotondo btnR;
    public static JButtonRotondo btnR1;
    private javax.swing.JButton btnRotore1meno;
    private javax.swing.JButton btnRotore1piu;
    private javax.swing.JButton btnRotore2meno;
    private javax.swing.JButton btnRotore2piu;
    private javax.swing.JButton btnRotore3meno;
    private javax.swing.JButton btnRotore3piu;
    private JButtonRotondo btnS;
    public static JButtonRotondo btnS1;
    private javax.swing.JButton btnSalva;
    private JButtonRotondo btnT;
    public static JButtonRotondo btnT1;
    private JButtonRotondo btnU;
    public static JButtonRotondo btnU1;
    private JButtonRotondo btnV;
    public static JButtonRotondo btnV1;
    private JButtonRotondo btnW;
    public static JButtonRotondo btnW1;
    private JButtonRotondo btnX;
    public static JButtonRotondo btnX1;
    private JButtonRotondo btnY;
    public static JButtonRotondo btnY1;
    private JButtonRotondo btnZ;
    public static JButtonRotondo btnZ1;
    private javax.swing.JScrollBar jScrollBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtRis;
    private javax.swing.JTextField txtRotore1;
    private javax.swing.JTextField txtRotore2;
    private javax.swing.JTextField txtRotore3;
    // End of variables declaration//GEN-END:variables
}
