import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JButton;
import javax.swing.Icon;

public class JButtonRotondo extends JButton 
{
    //Colore bordo
    private final Color bordi = new Color(255, 0, 0);
    //Colore BackGround
    private final Color bg = new Color(0, 0, 0);
    
    public JButtonRotondo() 
    {
        super(null, null);
        ridimensiona();
    }
    public JButtonRotondo(Icon icon) 
    {
        super(null, icon);
        ridimensiona();
    }
    public JButtonRotondo(String text) 
    {
        super(text, null);
        ridimensiona();
    }
    public JButtonRotondo(String text, Icon icon) 
    {
        super(text, icon);
        ridimensiona();
    }
    private void ridimensiona() 
    {
        Dimension d = getPreferredSize();
        d.width = d.height = Math.max(d.width, d.height);
        setPreferredSize(d);
        setContentAreaFilled(false);
        setBorderPainted(false);
        setForeground(new Color(255, 255, 255));
        setBackground(bg); 
    }
    @Override
    public void paintComponent(Graphics g) 
    {
        Color bg1 = getBackground();
        //Viene impostato il colore al click
        if (getModel().isPressed()) 
        {
            int red = bg1.getRed() + 50;
            int green = bg1.getGreen() + 50;
            int blue = bg1.getBlue() + 50;
            if (red < 0) red = 0;
            if (red > 255) red = 255;
            if (green < 0) green = 0;
            if (green > 255) green = 255;
            if (blue < 0) blue = 0;
            if (blue > 255) blue = 255;
            setBackground(new Color(red, green, blue));
        }
        //Renderizza il bottone
        ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //g.setColor(bordi);
        //g.fillOval(0, 0, getWidth(), getHeight());
        g.setColor(getBackground());
        g.fillOval(3, 3, getWidth() - 6, getHeight() - 6);
        //Viene impostato il colore predefinito
        setBackground(bg1);
        //Renderizza il testo
        super.paintComponent(g);
    }
}