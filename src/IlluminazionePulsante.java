import java.awt.Color;
public class IlluminazionePulsante implements Runnable 
{
    char lettera1;
    IlluminazionePulsante(char lettera)
    {
        lettera1=lettera;
    }
    @Override
    public void run()
    {
        long millisecondi_ora, millisecondi_fine, intervallo;
        
        millisecondi_ora = System.currentTimeMillis();
        intervallo = 250; 
        millisecondi_fine= millisecondi_ora+intervallo;
        if(lettera1=='Q')
            frmEnigma.btnQ1.setBackground(Color.yellow);
        else if(lettera1=='W')
            frmEnigma.btnW1.setBackground(Color.yellow);
        else if(lettera1=='E')
            frmEnigma.btnE1.setBackground(Color.yellow);
        else if(lettera1=='R')
            frmEnigma.btnR1.setBackground(Color.yellow);
        else if(lettera1=='T')
            frmEnigma.btnT1.setBackground(Color.yellow);
        else if(lettera1=='Y')
            frmEnigma.btnY1.setBackground(Color.yellow);
        else if(lettera1=='U')
            frmEnigma.btnU1.setBackground(Color.yellow);
        else if(lettera1=='I')
            frmEnigma.btnI1.setBackground(Color.yellow);
        else if(lettera1=='O')
            frmEnigma.btnO1.setBackground(Color.yellow);
        else if(lettera1=='P')
            frmEnigma.btnP1.setBackground(Color.yellow);
        else if(lettera1=='A')
            frmEnigma.btnA1.setBackground(Color.yellow);
        else if(lettera1=='S')
            frmEnigma.btnS1.setBackground(Color.yellow);
        else if(lettera1=='D')
            frmEnigma.btnD1.setBackground(Color.yellow);
        else if(lettera1=='F')
            frmEnigma.btnF1.setBackground(Color.yellow);
        else if(lettera1=='G')
            frmEnigma.btnG1.setBackground(Color.yellow);
        else if(lettera1=='H')
            frmEnigma.btnH1.setBackground(Color.yellow);
        else if(lettera1=='J')
            frmEnigma.btnJ1.setBackground(Color.yellow);
        else if(lettera1=='K')
            frmEnigma.btnK1.setBackground(Color.yellow);
        else if(lettera1=='L')
            frmEnigma.btnL1.setBackground(Color.yellow);
        else if(lettera1=='Z')
            frmEnigma.btnZ1.setBackground(Color.yellow);
        else if(lettera1=='X')
            frmEnigma.btnX1.setBackground(Color.yellow);
        else if(lettera1=='C')
            frmEnigma.btnC1.setBackground(Color.yellow);
        else if(lettera1=='V')
            frmEnigma.btnV1.setBackground(Color.yellow);
        else if(lettera1=='B')
            frmEnigma.btnB1.setBackground(Color.yellow);
        else if(lettera1=='N')
            frmEnigma.btnN1.setBackground(Color.yellow);
        else if(lettera1=='M')
            frmEnigma.btnM1.setBackground(Color.yellow);
        
        while(millisecondi_ora<millisecondi_fine)
            millisecondi_ora = System.currentTimeMillis();
        
       if(lettera1=='Q')
            frmEnigma.btnQ1.setBackground(Color.white);
        else if(lettera1=='W')
            frmEnigma.btnW1.setBackground(Color.white);
        else if(lettera1=='E')
            frmEnigma.btnE1.setBackground(Color.white);
        else if(lettera1=='R')
            frmEnigma.btnR1.setBackground(Color.white);
        else if(lettera1=='T')
            frmEnigma.btnT1.setBackground(Color.white);
        else if(lettera1=='Y')
            frmEnigma.btnY1.setBackground(Color.white);
        else if(lettera1=='U')
            frmEnigma.btnU1.setBackground(Color.white);
        else if(lettera1=='I')
            frmEnigma.btnI1.setBackground(Color.white);
        else if(lettera1=='O')
            frmEnigma.btnO1.setBackground(Color.white);
        else if(lettera1=='P')
            frmEnigma.btnP1.setBackground(Color.white);
        else if(lettera1=='A')
            frmEnigma.btnA1.setBackground(Color.white);
        else if(lettera1=='S')
            frmEnigma.btnS1.setBackground(Color.white);
        else if(lettera1=='D')
            frmEnigma.btnD1.setBackground(Color.white);
        else if(lettera1=='F')
            frmEnigma.btnF1.setBackground(Color.white);
        else if(lettera1=='G')
            frmEnigma.btnG1.setBackground(Color.white);
        else if(lettera1=='H')
            frmEnigma.btnH1.setBackground(Color.white);
        else if(lettera1=='J')
            frmEnigma.btnJ1.setBackground(Color.white);
        else if(lettera1=='K')
            frmEnigma.btnK1.setBackground(Color.white);
        else if(lettera1=='L')
            frmEnigma.btnL1.setBackground(Color.white);
        else if(lettera1=='Z')
            frmEnigma.btnZ1.setBackground(Color.white);
        else if(lettera1=='X')
            frmEnigma.btnX1.setBackground(Color.white);
        else if(lettera1=='C')
            frmEnigma.btnC1.setBackground(Color.white);
        else if(lettera1=='V')
            frmEnigma.btnV1.setBackground(Color.white);
        else if(lettera1=='B')
            frmEnigma.btnB1.setBackground(Color.white);
        else if(lettera1=='N')
            frmEnigma.btnN1.setBackground(Color.white);
        else if(lettera1=='M')
            frmEnigma.btnM1.setBackground(Color.white);
    }
}
  
